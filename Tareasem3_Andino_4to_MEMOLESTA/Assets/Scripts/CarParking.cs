using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class CarParking : MonoBehaviour
{
    public Animator transition;
    public GameObject imagevideo;
    public VideoPlayer videoPlayer;
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Car")
        {
            StartCoroutine("EndGame");
            
        }
    }

    IEnumerator EndGame()
    {
        transition.SetTrigger("End");
        yield return new WaitForSeconds(2);
        transition.SetTrigger("Start");
        imagevideo.SetActive(true);
        videoPlayer.Play();
        yield return new WaitForSeconds(21f);
        transition.SetTrigger("End");
        SceneManager.LoadScene("Menu");
    }

}
