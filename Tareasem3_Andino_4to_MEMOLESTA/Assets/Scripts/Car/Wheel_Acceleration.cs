using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wheel_Acceleration : MonoBehaviour
{
    RaycastHit hit;
    bool RayCastDidHit = false;

    Rigidbody rbCar;
    Transform carTransform;

    public float carTopSpeed = 1.0f;

    AnimationCurve powerCurve;

    public bool isDriving = false;

    void Start()
    {
        rbCar = gameObject.GetComponentInParent<Rigidbody>();
        carTransform = gameObject.GetComponentInParent<Transform>();        
    }

    void FixedUpdate()
    {
        RayCastDidHit = Physics.Raycast(transform.position, -transform.up, out hit, 1.5f);
        float inputHor = Input.GetAxis("Horizontal");
        if (RayCastDidHit)
        {
            Vector3 accelDir = transform.forward;

            float accelInput = Input.GetAxis("Vertical");

            if(isDriving)
            {
                if(accelInput < 0)
                {
                    rbCar.AddForceAtPosition(accelDir * accelInput * 160, transform.position);
                }
                if (accelInput > 0)
                {
                    rbCar.AddForceAtPosition(accelDir * accelInput * 100, transform.position);
                }
                transform.localRotation = (Quaternion.Euler(0, inputHor * 60, 0));
            }
            if(!isDriving)
            {
                rbCar.velocity = Vector3.zero;
            }
        }

    }
}
