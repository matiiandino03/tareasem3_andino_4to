using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirveCar : MonoBehaviour
{
    public Transform player;
    public Wheel_Acceleration leftWheel;
    public Wheel_Acceleration rightWheel;
    public GameObject carCamera;
    public List<Wheel_Steering> steerings = new List<Wheel_Steering>();
    Rigidbody rbCar;
    public Transform carSpawn;
    public GameObject arrow;
    private void Start()
    {
        rbCar = GetComponent<Rigidbody>();
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.E) && Vector3.Distance(player.position,transform.position)< 3)
        {
            leftWheel.isDriving = true;
            rightWheel.isDriving = true;
            player.gameObject.SetActive(false);
            carCamera.SetActive(true);
            arrow.SetActive(true);
            foreach(Wheel_Steering steering in steerings)
            {
                steering.isDriving = true;
            }
        }
        if(Input.GetKey(KeyCode.R))
        {
            transform.position = carSpawn.transform.position;
            transform.rotation = carSpawn.transform.rotation;
        }
    }
}
