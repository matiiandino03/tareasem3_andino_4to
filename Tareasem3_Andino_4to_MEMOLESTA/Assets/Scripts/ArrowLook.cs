using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowLook : MonoBehaviour
{
    public GameObject parking;

    private void Update()
    {
        transform.LookAt(parking.transform.position);
    }
}
