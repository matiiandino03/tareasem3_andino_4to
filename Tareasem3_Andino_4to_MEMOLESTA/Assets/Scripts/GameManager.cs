using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public int hora = 18;
    public int minutos = 30;
    public int segundos = 0;
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
        InvokeRepeating("timer", 0.25f, 0.25f);
    }

    void Update()
    {
        
    }

    public void timer()
    {
        segundos++;
        if(segundos == 60)
        {
            minutos++;
            segundos = 0;
        }
        if(minutos == 60)
        {
            minutos = 0;
            hora++;
        }
    }
}
