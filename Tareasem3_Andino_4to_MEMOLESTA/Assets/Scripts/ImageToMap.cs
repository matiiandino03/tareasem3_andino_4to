using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageToMap : MonoBehaviour
{
    [System.Serializable]
    public class ColorAPrefab
    {
        public Color color;
        public GameObject prefab;
    }

    public Texture2D mapa;
    public ColorAPrefab[] colorMappings;

    void Start()
    {
        GenerarNivel();
    }

    private void GenerarNivel()
    {
        for (int x = 0; x < mapa.width; x++)
        {
            for (int y = 0; y < mapa.height; y++)
            {
                GenerarTile(x, y);
            }
        }
    }

    void GenerarTile(int x, int y)
    {
        Color pixelColor = mapa.GetPixel(x, y);
        if (pixelColor.a == 0)
        {
            return;
        }

        foreach (ColorAPrefab colorMapping in colorMappings)
        {
            Debug.Log(colorMapping.color.ToString() + pixelColor.ToString());
            if (colorMapping.color.Equals(pixelColor))
            {
                Vector3 position = new Vector3(x, 0,y);
                Debug.Log("Instancie:" + colorMapping.prefab);
                Instantiate(colorMapping.prefab, position, Quaternion.identity, transform);
            }
        }
    }
}

