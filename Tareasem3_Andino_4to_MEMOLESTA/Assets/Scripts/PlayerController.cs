using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float rapidezDesplazamiento = 5.0f;
    public float rapidezDesplazamientoSprint = 1.0f;
    public float fuerzaSalto = 300f;
    GameObject Jugador;
    public GameObject CheckGround;
    public LayerMask mask;
    Rigidbody rb;
    bool jumpTimer = true;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        Move();
    }

    public void Move()
    {
        rb.isKinematic = false;
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            rapidezDesplazamiento += rapidezDesplazamientoSprint;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            rapidezDesplazamiento = 5.0f;
        }

        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;


        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown(KeyCode.Space) && Physics.OverlapSphere(CheckGround.transform.position, 1, mask).Length > 0 && jumpTimer)
        {
            GetComponent<Rigidbody>().AddForce(Vector3.up * fuerzaSalto);
            jumpTimer = false;
            StartCoroutine("WaitForJump");
        }
    }

    IEnumerator WaitForJump()
    {
        yield return new WaitForSeconds(1.3f);
        jumpTimer = true;
    }

}
