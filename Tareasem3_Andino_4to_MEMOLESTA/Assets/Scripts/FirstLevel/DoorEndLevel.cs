using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorEndLevel : MonoBehaviour
{
    public bool endedMission = false;
    public GameObject positionPlayerLvl2;
    public GameObject player;
    public GameObject secondMission;
    public GameObject thirdMission;
    public Animator transition;
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player") && endedMission)
        {
            StartCoroutine("ChangeLvl");
        }
    }

    IEnumerator ChangeLvl()
    {
        secondMission.SetActive(false);
        transition.SetTrigger("End");
        yield return new WaitForSeconds(1.9f);
        player.transform.position = positionPlayerLvl2.transform.position;
        yield return new WaitForSeconds(0.1f);
        transition.SetTrigger("Start");
        thirdMission.SetActive(true);
    }

}
