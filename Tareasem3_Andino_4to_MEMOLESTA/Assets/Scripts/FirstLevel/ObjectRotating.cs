using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotating : MonoBehaviour
{
    public bool rotateZ;
    void Update()
    {
        if(rotateZ)
        {
            transform.Rotate(0, 0,25 * Time.deltaTime, Space.Self);
        }
        else
        {
            transform.Rotate(0, 25 * Time.deltaTime, 0, Space.Self);
        }
    }
}
