using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WardrobeDoor : MonoBehaviour
{
    public Transform Player;
    public bool done = false;
    public bool opening = false;
    private void Update()
    {
        if(!done && Vector3.Distance(Player.position,transform.position) < 5f && Input.GetKeyDown(KeyCode.E))
        {           
            done = true;
            opening = true;
        }
        if(opening)
        {
            if(transform.localPosition.x < 2.6f)
            {
                transform.Translate(3f*Time.deltaTime, 0, 0,Space.World);
            }
        }
    }
    
}
