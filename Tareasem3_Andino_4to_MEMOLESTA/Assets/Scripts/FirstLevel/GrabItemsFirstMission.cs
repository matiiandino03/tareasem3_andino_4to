using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GrabItemsFirstMission : MonoBehaviour
{
    public GameObject waterBottle;
    public GameObject Botines;
    public GameObject camara;

    public Toggle waterToggle;
    public Toggle botinesToggle;

    public GameObject firstMission;
    public GameObject secondMission;
    public GameObject misionCumplida;

    bool mision1ended = false;

    public Text txtTiempo;

    public DoorEndLevel endLvl;
    void Update()
    {
        if(Vector3.Distance(camara.transform.position,waterBottle.transform.position) < 1.3f && Input.GetKeyDown(KeyCode.E))
        {
            waterToggle.isOn = true;
            waterBottle.SetActive(false);
        }
        if (Vector3.Distance(camara.transform.position, Botines.transform.position) < 1.3f && Input.GetKeyDown(KeyCode.E))
        {
            botinesToggle.isOn = true;
            Botines.SetActive(false);
        }
        if(waterToggle.isOn && botinesToggle.isOn && !mision1ended)
        {
            StartCoroutine("mision1");
            mision1ended = true;
        }
        if(GameManager.instance.minutos <10)
        {
            txtTiempo.text = GameManager.instance.hora.ToString() + ":0" + GameManager.instance.minutos.ToString();
        }
        if (GameManager.instance.minutos > 10)
        {
            txtTiempo.text = GameManager.instance.hora.ToString() + ":" + GameManager.instance.minutos.ToString();
        }
            
    }

    IEnumerator mision1()
    {
        yield return new WaitForSeconds(0.5f);
        firstMission.SetActive(false);
        misionCumplida.SetActive(true);
        yield return new WaitForSeconds(1f);
        misionCumplida.SetActive(false);
        secondMission.SetActive(true);
        endLvl.endedMission = true;
    }
}
