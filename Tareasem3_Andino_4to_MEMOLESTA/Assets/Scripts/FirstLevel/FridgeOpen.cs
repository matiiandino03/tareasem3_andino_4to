using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FridgeOpen : MonoBehaviour
{
    public Transform Player;
    public bool done = false;
    public bool opening = false;
    private void Update()
    {
        if (!done && !opening && Vector3.Distance(Player.position, transform.position) < 2f && Input.GetKeyDown(KeyCode.E))
        {
            opening = true;
        }
        if(opening && transform.localRotation.z > -90)
        {
            transform.Rotate(0, 0, -40 * Time.deltaTime, Space.Self);
            done = true;           
        }
        if(transform.localRotation.z <= -0.5f)
        {
            opening = false;
        }
    }
}
