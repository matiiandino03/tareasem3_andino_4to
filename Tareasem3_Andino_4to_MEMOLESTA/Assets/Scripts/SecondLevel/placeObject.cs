using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class placeObject : MonoBehaviour
{
    public GameObject ghostObjectBidon;
    public GameObject placedObjectBidon;

    public GameObject ghostObjectBox;
    public GameObject placedObjectBox;

    GameObject holdingObject = null;
    GameObject currentGhost = null;
    public bool holding = false;

    public Transform hands;

    public ObjectsInZone objectsInZone;

    void Update()
    {
        RaycastHit hit;
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (holdingObject == null)
            {
                RaycastHit hit2;
                if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit2, 2))
                {
                    if (hit2.transform.gameObject.tag == "Bidon" || hit2.transform.gameObject.tag == "Box")
                    {
                        if(hit2.transform.gameObject.tag == "Bidon")
                        {
                            hit2.transform.gameObject.transform.position = new Vector3(hands.transform.position.x, hands.transform.position.y -1, hands.transform.position.z);
                        }
                        if (hit2.transform.gameObject.tag == "Box")
                        {
                            hit2.transform.gameObject.transform.position = new Vector3(hands.transform.position.x, hands.transform.position.y - 0.5f, hands.transform.position.z);
                        }
                        hit2.transform.gameObject.transform.parent = hands.transform;
                        holding = true;
                        holdingObject = hit2.transform.gameObject;
                        holdingObject.GetComponent<Rigidbody>().isKinematic = true;
                        holdingObject.GetComponentInChildren<BoxCollider>().enabled = false;
                        if(holdingObject.GetComponent<Rigidbody>().mass == 1.01f)
                        {
                            holdingObject.GetComponent<Rigidbody>().mass = 1f;
                            objectsInZone.objectsInside--;
                        }
                    }
                }
            }
        }
        if (holding)
            {
                if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
                {
                    if (holdingObject.tag == "Bidon")
                    {
                        if (currentGhost == null)
                        {
                            currentGhost = Instantiate(ghostObjectBidon, hit.point, Quaternion.identity);
                        }
                        currentGhost.transform.position = hit.point;
                        if (Input.GetKey(KeyCode.R))
                        {
                            currentGhost.transform.Rotate(0, 50 * Time.deltaTime, 0, Space.Self);
                        }
                        if (Input.GetMouseButtonDown(0))
                        {
                            Instantiate(placedObjectBidon, currentGhost.transform.position, currentGhost.transform.rotation);
                            Destroy(currentGhost);
                            currentGhost = null;
                            Destroy(holdingObject);
                            holdingObject = null;
                            holding = false;
                            return;
                        }
                    }
                    if (holdingObject.tag == "Box")
                    {
                        if (currentGhost == null)
                        {
                            currentGhost = Instantiate(ghostObjectBox, hit.point, Quaternion.identity);
                        }
                        currentGhost.transform.position = hit.point;
                        if (Input.GetKey(KeyCode.R))
                        {
                            currentGhost.transform.Rotate(0, 50 * Time.deltaTime, 0, Space.Self);
                        }
                        if (Input.GetMouseButtonDown(0))
                        {
                            Instantiate(placedObjectBox, currentGhost.transform.position, currentGhost.transform.rotation);
                            Destroy(currentGhost);
                            currentGhost = null;
                            Destroy(holdingObject);
                            holdingObject = null;
                            holding = false;
                        }
                    }

                }
        }
    }


}

