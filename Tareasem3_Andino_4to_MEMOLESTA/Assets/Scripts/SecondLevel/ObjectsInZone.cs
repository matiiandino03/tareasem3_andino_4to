using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsInZone : MonoBehaviour
{
    public int objectsInside = 0;
    public GameObject garageDoor;
    bool openDoor = false;

    public GameObject thirdmission;
    public GameObject fourthMission;

    public DirveCar driveCar;

    public List<GameObject> zone = new List<GameObject>();
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Bidon")|| other.gameObject.CompareTag("Box"))
        {
            objectsInside++;
            other.gameObject.GetComponentInParent<Rigidbody>().mass = 1.01f;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Bidon") || other.gameObject.CompareTag("Box"))
        {
            objectsInside--;
            other.gameObject.GetComponentInParent<Rigidbody>().mass = 1f;
        }
    }

    private void Update()
    {
        if(objectsInside == 16)
        {
            openDoor = true;
            thirdmission.SetActive(false);
            fourthMission.SetActive(true);
            driveCar.enabled = true;
            foreach(GameObject g in zone)
            {
                g.SetActive(false);
            }
        }
        else
        {
            openDoor = false;
            driveCar.enabled = false;
            foreach (GameObject g in zone)
            {
                g.SetActive(true);
            }
        }

        if (openDoor && garageDoor.transform.rotation.x > -0.7f)
        {
            garageDoor.transform.Rotate(-40 * Time.deltaTime, 0, 0, Space.Self);
        }
        if (!openDoor && garageDoor.transform.rotation.x < 0)
        {
            garageDoor.transform.Rotate(40 * Time.deltaTime, 0, 0, Space.Self);
        }
    }
}
